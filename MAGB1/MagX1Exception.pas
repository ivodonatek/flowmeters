unit MagX1Exception;

interface

uses Sysutils;

type  EUnsuportedFirmwareException = class(Exception);
type  ECommunicationPortError = class(Exception);
type ENoDeviceDetected = class(Exception);
type EReadingError = class(Exception);
type EWritingError = class(Exception);

implementation

end.
 