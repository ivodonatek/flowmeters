unit MagBValueIHDTFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MagBValueIHexFrame, ExtCtrls, StdCtrls;

// IHDT = Info Hex DateTime

type
  TframeMagBValueIHDT = class(TframeMagBValueIHex)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetValueText(Value:Integer): String; override;
  end;

implementation

{$R *.DFM}

{ TframeMagBValueIHDT }

function TframeMagBValueIHDT.GetValueText(Value: Integer): String;
begin
  Result := Format('%.8x', [Value]);
  Insert(':', Result, 7);
  Insert(' ', Result, 5);
  Insert('/', Result, 3);
end;

end.
