unit Constants;

interface

type
  TX = record
        Ch : Char;
        B  : Byte;
      end;

const
  // Kazdy znak se musi napsat do pole ve tvaru ( Ch:'puvodni znak';B:kod v nasi znakove sade )
  //   po pridani znaku je nutne zvysit hodnotu velikosti pole ... array[1..XXX] ...
  //   pro predhlednost je pole rozdeleno do sekci pro jednotlive jazyky (pouze pro prehlednost)
  //   pro spravne nahrazeni znaku je nutne aby znak vypadal stejne v EXCELu v DELPHach
  Znaky : array[1..72] of TX = (

    // TO DO : doplnit tabulku znaku ... - soucasna data jsou pouze testovaci !!!

    //CZ - 19 - �������������������
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65), (Ch:'�';B:65),
    //DE -  7 - �������
    (Ch:'�';B:66), (Ch:'�';B:66), (Ch:'�';B:66), (Ch:'�';B:66), (Ch:'�';B:66),
    (Ch:'�';B:66), (Ch:'�';B:66),
    //FR - 22 - ����������������������
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67), (Ch:'�';B:67),
    (Ch:'�';B:67), (Ch:'�';B:67),
    //ES - 24 - ������������������������
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68),
    (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68), (Ch:'�';B:68)
  );

implementation                 

end.
 