program MagB1;

uses
  Forms,
  Controls,
  sysutils,
  Dialogs,
  EnterFrm in 'EnterFrm.pas' {formEnter},
  ModbusM in 'ModbusM.pas',
  MyCommunicationClass in 'MyCommunicationClass.pas',
  FunctionsUnit in 'FunctionsUnit.pas',
  MagBHeadFrm in 'MagBHeadFrm.pas' {formMagBHead},
  WaitFrm in 'WaitFrm.pas' {formWait},
  MagBMenuFrm in 'MagBMenuFrm.pas' {formMagBMenu},
  ChangeValueFrame in 'ChangeValueFrame.pas' {frameValueChange: TFrame},
  MagBValueHeadFrame in 'MagBValueHeadFrame.pas' {frameMagBHead: TFrame},
  MagBValueInfoFrame in 'MagBValueInfoFrame.pas' {frameMagBValueInfo: TFrame},
  MagBValueEditFrame in 'MagBValueEditFrame.pas' {frameMagBValueEdit: TFrame},
  MagBValue4WinFrame in 'MagBValue4WinFrame.pas' {frameMagBValue4Win: TFrame},
  MagBValueBoolFrame in 'MagBValueBoolFrame.pas' {frameMagBValueBool: TFrame},
  MagBValueISelFrame in 'MagBValueISelFrame.pas' {frameMagBValueISel: TFrame},
  MagBValuePaswFrame in 'MagBValuePaswFrame.pas' {frameMagBValuePasw: TFrame},
  MagBValueIHexFrame in 'MagBValueIHexFrame.pas' {frameMagBValueIHex: TFrame},
  MagBValueIBinFrame in 'MagBValueIBinFrame.pas' {frameMagBValueIBin: TFrame},
  MagBValueIHDTFrame in 'MagBValueIHDTFrame.pas' {frameMagBValueIHDT: TFrame},
  MagBPassFrm in 'MagBPassFrm.pas' {formMagBPassword},
  NodeClass in 'NodeClass.pas',
  Constants in 'Constants.pas',
  MenuNodeClass in 'MenuNodeClass.pas',
  DataClass in 'DataClass.pas',
  mxCalendar in 'mxCalendar.pas',
  MagX1Exception in 'MagX1Exception.pas',
  GlobalUtilsClass in 'GlobalUtilsClass.pas',
  PassForm in 'PassForm.pas' {formPassword},
  MagBStatFrm in 'MagBStatFrm.pas' {formMagBStat},
  MagBStatRpt in 'MagBStatRpt.pas' {qrMagBStat: TQuickRep},
  TranslationUnit in 'TranslationUnit.pas';

{$R *.RES}

var formEnter : TformEnter;
    Demo : Boolean;
    ReadOK:Boolean;
    firmwareNO:integer;
    MyCommunication : TMyCommunication;

function GetDeviceFirmwareNumber(AProtokolType:TProtokolType;ASlaveID,AComNumber:Cardinal;
   BaudRate:TBaudRate;StopBits:TStopBits;Parity:TParity;Timeout:Integer;
   RtsControl:TRtsControl;EnableDTROnOpen:Boolean;ASwitchWait:Cardinal;
   AIPAddress: String; APort: Integer):integer;
var firmwareNO:integer;
begin
    MyCommunication.SetCommunicationParam(AComNumber,ASlaveID,BaudRate,db8BITS,
        StopBits,Parity,RtsControl,EnableDTROnOpen,Timeout,AIPAddress,APort);
    if not MyCommunication.Connect() then
        raise Exception.Create('Comunication port error');
    //cti verzi FW
    repeat
      ReadOK:=MyCommunication.ReadFirmNO(Cardinal(firmwareNO));
    until (ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR), mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if not ReadOK then
        raise Exception.Create('No device was detected');
    Result:=firmwareNO;
end;

begin
  Demo:=false;
  Repeat
    Application := TApplication.Create (nil);
    Application.Initialize;
//    Application.Title := 'MagX1';
    Application.Title := 'MagB1';
    formEnter:=TformEnter.Create(nil);
  	Konec:=false;
    formEnter.DemoCheckBox.Checked:=Demo;

    try
      case formEnter.ShowModal of
        //statistic
        mrOK:
        begin
              case formEnter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formEnter.ProtokolType,formEnter.SlaveID,
                      formEnter.ComPortNumber,formEnter.BaudRate,formEnter.StopBits,
                      formEnter.Parity,formEnter.Timeout,formEnter.RtsControl ,true,
                      formEnter.rgConvertorRS485.ItemIndex, formEnter.edtTCPIPAddress.Text,
                      formEnter.seTCPPort.Value);
              //MagB
              Application.Title := 'MagB1';
              Application.CreateForm(TformMagBStat, formMagBStat);
  formMagBStat.Init(MyCommunication, firmwareNO);
              Application.Run;
        end;
        //Service
        mrNo:
        begin
              case formEnter.ProtokolType of
                ptMagX1:MyCommunication:=TMyCommPort.Create();
                ptModbus:MyCommunication:=TMyModbus.Create();
                ptTCPModbus:MyCommunication:=TMyTCPModbus.Create();
                ptDemo:MyCommunication:=TDemoCommunication.Create();
              else
                raise Exception.Create('Unsuported protocol');
              end;
              //cti verzi firmware
              firmwareNO := GetDeviceFirmwareNumber(formEnter.ProtokolType,formEnter.SlaveID,
                      formEnter.ComPortNumber,formEnter.BaudRate,formEnter.StopBits,
                      formEnter.Parity,formEnter.Timeout,formEnter.RtsControl ,true,
                      formEnter.rgConvertorRS485.ItemIndex, formEnter.edtTCPIPAddress.Text,
                      formEnter.seTCPPort.Value);
              //MagB
              Application.Title := 'MagB1';
              Application.CreateForm(TformMagBMenu, formMagBMenu);
              formMagBMenu.Init(MyCommunication, firmwareNO, formEnter.ProtokolType);
              Application.Run;
        end;
        //Close
        mrCancel:
        begin
          Application.Terminate;
          Konec := True;
        end;
      end;

    except
      on E: Exception do
      begin
        MessageDlg(E.Message,mtError, [mbOk], 0);
//        Application.Terminate();
      end;
    end;
    Demo:=formEnter.DemoCheckBox.Checked;
    formEnter.Free;
    Application.ProcessMessages();
    Application.MainForm.Free;
    if Assigned(MyCommunication) then
    	FreeAndNil(MyCommunication);
  Until (Konec);
end.

