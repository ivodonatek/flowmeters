; -- MagB1.iss --

[Setup]
AppName=MagB1
AppVerName=MagB1 1.0.0.25
AppPublisher=Arkon Flow Systems, s.r.o.
AppVersion=1.0.0.25
VersionInfoVersion=1.0.0.25
DefaultDirName={pf}\Arkon Flow Systems\MagB1
DefaultGroupName=Arkon Flow Systems\MagB1
UninstallDisplayIcon={app}\MagB1.exe
Compression=lzma
SolidCompression=yes

[Files]
Source: "MagB1.exe"; DestDir: "{app}"
Source: "MagB1.ico"; DestDir: "{app}"
Source: "MagB1_Error.dat"; DestDir: "{app}"
Source: "Menu1022.dat"; DestDir: "{app}"
Source: "Modbus1022.dat"; DestDir: "{app}"
Source: "Background.jpg"; DestDir: "{app}"
Source: "MagLogo.jpg"; DestDir: "{app}"
Source: "Logo.jpg"; DestDir: "{app}"
Source: "DelZip179.dll"; DestDir: "{app}"
Source: "MagB1.dic"; DestDir: "{app}"
Source: "DemoValues.dat"; DestDir: "{userappdata}\MagB1"

[Icons]
Name: "{group}\MagB1 SW"; Filename: "{app}\MagB1.exe"

[Run]
Filename: "{app}\MagB1.exe"; Description: "Launch application"; Flags: postinstall nowait skipifsilent unchecked
