; -- AgrimagP2.iss --

[Setup]
AppName=AgrimagP2
AppVerName=AgrimagP2 1.0.0.1
AppPublisher=Arkon Flow Systems, s.r.o.
AppVersion=1.0.0.1
VersionInfoVersion=1.0.0.1
DefaultDirName={pf}\Arkon Flow Systems\AgrimagP2
DefaultGroupName=Arkon Flow Systems\AgrimagP2
UninstallDisplayIcon={app}\AgrimagP2.exe
Compression=lzma
SolidCompression=yes

[Files]
Source: "AgrimagP2.exe"; DestDir: "{app}"
Source: "AgrimagP2.ico"; DestDir: "{app}"
Source: "AgrimagP2_Error.dat"; DestDir: "{app}"
Source: "Menu1022.dat"; DestDir: "{app}"
Source: "Modbus1022.dat"; DestDir: "{app}"
Source: "Menu5007.dat"; DestDir: "{app}"
Source: "Modbus5007.dat"; DestDir: "{app}"
Source: "Background.jpg"; DestDir: "{app}"
Source: "MagLogo.jpg"; DestDir: "{app}"
Source: "Logo.jpg"; DestDir: "{app}"
Source: "DelZip179.dll"; DestDir: "{app}"
Source: "AgrimagP2.dic"; DestDir: "{app}"
Source: "DemoValues.dat"; DestDir: "{userappdata}\AgrimagP2"

[Icons]
Name: "{group}\AgrimagP2 SW"; Filename: "{app}\AgrimagP2.exe"

[Run]
Filename: "{app}\AgrimagP2.exe"; Description: "Launch application"; Flags: postinstall nowait skipifsilent unchecked
