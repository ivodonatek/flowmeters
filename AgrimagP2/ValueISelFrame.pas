unit ValueISelFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MyCommunicationClass, NodeClass,
  ExtCtrls, Buttons, WaitFrm, FunctionsUnit, MagBValueHeadFrame;

type
  TframeValueISel = class(TframeMagBHead)
    Panel1: TPanel;
    cbValues: TComboBox;
    btnWrite: TButton;
    procedure btnWriteClick(Sender: TObject);
  protected
  private
    { Private declarations }
    procedure FillList;
  public
    { Public declarations }
    function GetValueText(FourByte : T4Byte) : String;
    constructor CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode);
  end;

var
  frameValueISel: TframeValueISel;

implementation

{$R *.DFM}

{ TframeValueISel }


constructor TframeValueISel.CreateFrame(AOwner: TComponent; MyCommunication : TMyCommunication;
    EditNode : TNode);
var ReadOK:Boolean;
  FourByte:T4Byte;
  WaitFrm : TformWait;
begin
  inherited;
  FillList;

  WaitFrm:=TformWait.Create(Self);
  try
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    Screen.Cursor:=crHourGlass;
    repeat
      Application.ProcessMessages;
      ReadOK:=ReadValue(FEditNode.ValueName,FourByte);
    until(ReadOK)or(MessageDlg(GetTranslationText('READ_ERROR', STR_READ_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
    if ReadOK then
      cbValues.ItemIndex:=Integer(FourByte);
    WaitFrm.gWait.Progress:=100;
    Application.ProcessMessages;
  finally
    Screen.Cursor:=crDefault;
    WaitFrm.Free;
  end;
end;

procedure TframeValueISel.FillList;
var I : Integer;
begin
  cbValues.Clear;
  for I:=1 to FEditNode.MultiTexts.Count-1 do begin
    if (TNodeText(FEditNode.MultiTexts.Items[I]).MultiText.Count>0) then begin
      cbValues.Items.Add(TNodeText(FEditNode.MultiTexts.Items[I]).MultiText[0]);
    end else begin
      cbValues.Items.Add('unknown');
    end;
  end;
end;

function TframeValueISel.GetValueText(FourByte: T4Byte): String;
begin
  if (FEditNode.MultiTexts.Count>Integer(FourByte)) and (TNodeText(FEditNode.MultiTexts.Items[Integer(FourByte)]).MultiText.Count>0) then begin
    Result:=TNodeText(FEditNode.MultiTexts.Items[Integer(FourByte)]).MultiText[0];
  end else begin
    Result:='unknown';
  end;
end;

procedure TframeValueISel.btnWriteClick(Sender: TObject);
var FourByte : T4Byte;
    WriteOK:Boolean;
    WaitFrm : TformWait;
begin
  WaitFrm:=TformWait.Create(Self);
  try
    WaitFrm.gWait.Progress:=0;
    WaitFrm.Show;
    SetCenter(WaitFrm);
    Screen.Cursor:=crHourGlass;

    Integer(FourByte):=cbValues.ItemIndex;
    repeat
      Application.ProcessMessages;
      WriteOK:=WriteValue(FEditNode.ValueName,FourByte);
    until (WriteOK)or(MessageDlg(GetTranslationText('WRITE_ERROR', STR_WRITE_ERROR),mtError, [mbRetry, mbAbort], 0) = mrAbort);
//    if not WriteOK then;
    WaitFrm.gWait.Progress:=100;
    Application.ProcessMessages;
  finally
    Screen.Cursor:=crDefault;
    WaitFrm.Free;
  end;
end;

end.
