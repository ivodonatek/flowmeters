unit LogUnit;

interface

procedure WriteLog(const Msg: String);

implementation

uses Forms, SysUtils;

var LogHandle: Integer;

procedure WriteLine;
var
  LogMsg: String;
begin
  if LogHandle <> -1 then begin
    LogMsg := '-----------------------------------------------------------------'#13#10;
    FileWrite(LogHandle, LogMsg[1], Length(LogMsg));
  end;
end;

procedure WriteLog(const Msg: String);
var
  LogMsg: String;
begin
  if LogHandle <> -1 then begin
    LogMsg := FormatDateTime('hh:nn:ss:zzz', Now) + ' ' + Msg + #13#10;
    FileWrite(LogHandle, LogMsg[1], Length(LogMsg));
  end;
end;

procedure OpenLog;
var
  LogName: String;
begin
  try
    LogName := ChangeFileExt(Application.ExeName, '_' + FormatDateTime('yyyymmdd', Now) + '.log');
    if not FileExists(LogName) then begin
      LogHandle := FileCreate(LogName);
      FileClose(LogHandle);
    end;
    LogHandle := FileOpen(LogName, fmOpenWrite or fmShareDenyNone);
    FileSeek(LogHandle, 0, 2);
    WriteLine;
    WriteLog('[LOG START] - ' + FormatDateTime('dd/mm/yyyy', Now));
    WriteLine;
  except
    LogHandle := -1;
  end;
end;

initialization
  LogHandle := -1;
  OpenLog;

finalization
  FileClose(LogHandle);

end.
