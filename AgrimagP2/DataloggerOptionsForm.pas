unit DataloggerOptionsForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  Buttons, ExtCtrls, GlobalUtilsClass, Mask, MyCommunicationClass, Spin,
  PBSpinEdit, PBNumEdit, PBSuperSpin, ComCtrls;

type
  TformDataloggerOptions= class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    btnConfirm: TButton;
    btnCancel: TButton;
    chkDateTimeMin: TCheckBox;
    chkDateTimeMax: TCheckBox;
    chkRecordCountMax: TCheckBox;
    editRecordCountMax: TPBSpinEdit;
    dtpDateMin: TDateTimePicker;
    dtpTimeMin: TDateTimePicker;
    dtpDateMax: TDateTimePicker;
    dtpTimeMax: TDateTimePicker;
    procedure btnCancelClick(Sender: TObject);
    procedure btnConfirmClick(Sender: TObject);
    constructor CreateFrm(AOwner: TComponent; ADataloggerFilter: TDataloggerFilter);
    procedure chkDateTimeMinClick(Sender: TObject);
    procedure chkDateTimeMaxClick(Sender: TObject);
    procedure chkRecordCountMaxClick(Sender: TObject);
  private
    { Private declarations }
    FDataloggerFilter: TDataloggerFilter;
    procedure LoadUIState();
    procedure GetUIState();    
    procedure UpdateUIState();
  public
    { Public declarations }
  end;

implementation

uses TranslationUnit;

{$R *.DFM}

constructor TformDataloggerOptions.CreateFrm(AOwner: TComponent; ADataloggerFilter: TDataloggerFilter);
begin
  inherited Create(AOwner);
  FDataloggerFilter := ADataloggerFilter;
  LoadUIState();
  UpdateUIState();
end;

procedure TformDataloggerOptions.btnCancelClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TformDataloggerOptions.btnConfirmClick(Sender: TObject);
begin
  GetUIState();
  ModalResult:=mrOk;
end;

procedure TformDataloggerOptions.LoadUIState();
begin
  chkDateTimeMin.Checked := FDataloggerFilter.DateTimeMinEnabled;
  chkDateTimeMax.Checked := FDataloggerFilter.DateTimeMaxEnabled;
  chkRecordCountMax.Checked := FDataloggerFilter.RecordCountMaxEnabled;

  dtpDateMin.DateTime := FDataloggerFilter.DateTimeMin;
  dtpTimeMin.DateTime := FDataloggerFilter.DateTimeMin;

  dtpDateMax.DateTime := FDataloggerFilter.DateTimeMax;
  dtpTimeMax.DateTime := FDataloggerFilter.DateTimeMax;

  editRecordCountMax.Value := FDataloggerFilter.RecordCountMax;
end;

procedure TformDataloggerOptions.GetUIState();
var Year, Month, Day: Word;
  Hour, Min, Sec, MSec: Word;
begin
  FDataloggerFilter.DateTimeMinEnabled := chkDateTimeMin.Checked;
  FDataloggerFilter.DateTimeMaxEnabled := chkDateTimeMax.Checked;
  FDataloggerFilter.RecordCountMaxEnabled := chkRecordCountMax.Checked;

  DecodeDate(dtpDateMin.Date, Year, Month, Day);
  DecodeTime(dtpTimeMin.Time, Hour, Min, Sec, MSec);
  FDataloggerFilter.DateTimeMin := EncodeDate(Year, Month, Day) + EncodeTime(Hour, Min, Sec, 0);

  DecodeDate(dtpDateMax.Date, Year, Month, Day);
  DecodeTime(dtpTimeMax.Time, Hour, Min, Sec, MSec);
  FDataloggerFilter.DateTimeMax := EncodeDate(Year, Month, Day) + EncodeTime(Hour, Min, Sec, 0);

  FDataloggerFilter.RecordCountMax := editRecordCountMax.Value;
end;

procedure TformDataloggerOptions.UpdateUIState();
begin
  dtpDateMin.Enabled := chkDateTimeMin.Checked;
  dtpTimeMin.Enabled := chkDateTimeMin.Checked;

  dtpDateMax.Enabled := chkDateTimeMax.Checked;
  dtpTimeMax.Enabled := chkDateTimeMax.Checked;

  editRecordCountMax.Enabled := chkRecordCountMax.Checked;
end;

procedure TformDataloggerOptions.chkDateTimeMinClick(Sender: TObject);
begin
  UpdateUIState();
end;

procedure TformDataloggerOptions.chkDateTimeMaxClick(Sender: TObject);
begin
  UpdateUIState();
end;

procedure TformDataloggerOptions.chkRecordCountMaxClick(Sender: TObject);
begin
  UpdateUIState();
end;

end.
