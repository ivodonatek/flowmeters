unit WaitFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Gauges, StdCtrls, jpeg;

type
  TformWait = class(TForm)
    Image1: TImage;
    gWait: TGauge;
    imgLogo: TImage;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formWait: TformWait;

implementation

uses EnterFrm;

{$R *.DFM}

procedure TformWait.FormCreate(Sender: TObject);
begin
  imgLogo.Picture.LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName)) + LOGO_FILE_NAME);
end;

end.
