�
 TFORMDATALOGGEROPTIONS 0N  TPF0TformDataloggerOptionsformDataloggerOptionsLeft� Top� BorderIcons BorderStylebsDialogCaptionOptionsClientHeight� ClientWidth�Color�� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameVerdana
Font.Style OldCreateOrderPixelsPerInch`
TextHeight 	TGroupBox	GroupBox1LeftTopWidth�Height� CaptionRecord Filter SettingTabOrder  TPanelPanel1LeftTopWidth�Heightu
BevelOuter	bvLoweredColor��� TabOrder  	TCheckBoxchkDateTimeMinLeft
Top
Width� HeightCaptionFirst record timeTabOrder OnClickchkDateTimeMinClick  	TCheckBoxchkDateTimeMaxLeft
Top*Width� HeightCaptionLast record timeTabOrderOnClickchkDateTimeMaxClick  	TCheckBoxchkRecordCountMaxLeft
TopZWidth� HeightCaptionMaximum number of recordsTabOrderOnClickchkRecordCountMaxClick  TPBSpinEditeditRecordCountMaxLeft� TopXWidthyHeightCursor	crDefaultMaxValue���MinValue TabOrderValue 	AlignmenttaLeftJustify  TDateTimePicker
dtpDateMinLeft� TopWidthyHeightCalAlignmentdtaLeftDate �䏲���@Time �䏲���@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputParentShowHintShowHintTabOrder  TDateTimePicker
dtpTimeMinLeftPTopWidthyHeightCalAlignmentdtaLeftDate �䏲���@Time �䏲���@
DateFormatdfShortDateMode
dmComboBoxKinddtkTime
ParseInputParentShowHintShowHintTabOrder  TDateTimePicker
dtpDateMaxLeft� Top(WidthyHeightCalAlignmentdtaLeftDate �䏲���@Time �䏲���@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputParentShowHintShowHintTabOrder  TDateTimePicker
dtpTimeMaxLeftPTop(WidthyHeightCalAlignmentdtaLeftDate �䏲���@Time �䏲���@
DateFormatdfShortDateMode
dmComboBoxKinddtkTime
ParseInputParentShowHintShowHintTabOrder    TButton
btnConfirmLeft� Top� WidthIHeightCaptionOKDefault	TabOrderOnClickbtnConfirmClick  TButton	btnCancelLeftTop� WidthIHeightCancel	CaptionCancelTabOrderOnClickbtnCancelClick   