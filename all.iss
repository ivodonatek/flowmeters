; -- MagB1.iss --

[Setup]
AppName=Flowmeters
AppVerName=Flowmeters 1.0.0.24
AppPublisher=Arkon Flow Systems, s.r.o.
AppVersion=1.0.0.24
VersionInfoVersion=1.0.0.24
DefaultDirName={pf}\Arkon Flow Systems\
DisableDirPage=no
Disablewelcomepage=no
DefaultGroupName=Arkon Flow Systems\
UninstallDisplayIcon={app}\
Compression=lzma
SolidCompression=yes

[Files]
Source: MAGB1\MagB1.exe; DestDir: {app}\MAGB1
Source: MAGB1\MagB1.ico; DestDir: {app}\MAGB1
Source: MAGB1\MagB1_Error.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1022.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1022.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1023.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1023.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1024.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1024.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1025.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1025.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1026.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1026.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1027.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1027.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1028.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1028.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1029.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1029.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1030.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1030.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1031.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1031.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu1032.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus1032.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu3001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus3001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu4000.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus4000.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu4001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus4001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5001.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5003.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5003.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5004.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5004.dat; DestDir: {app}\MAGB1
Source: MAGB1\Menu5006.dat; DestDir: {app}\MAGB1
Source: MAGB1\Modbus5006.dat; DestDir: {app}\MAGB1
Source: MAGB1\Background.jpg; DestDir: {app}\MAGB1
Source: MAGB1\MagLogo.jpg; DestDir: {app}\MAGB1
Source: MAGB1\Logo.jpg; DestDir: {app}\MAGB1
Source: MAGB1\MagB1.dic; DestDir: {app}\MAGB1
Source: MAGB1\DemoValues.dat; DestDir: {userappdata}\MAGB1

Source: MAGX1\MagX1.exe; DestDir: {app}\MAGX1
Source: MAGX1\MagX1.ico; DestDir: {app}\MAGX1
Source: MAGX1\Menu105.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu106.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu107.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu110.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu111.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu112.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu113.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu200.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu201.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu203.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu210.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu211.dat; DestDir: {app}\MAGX1
Source: MAGX1\Menu212.dat; DestDir: {app}\MAGX1
Source: MAGX1\Values105.dat; DestDir: {app}\MAGX1
Source: MAGX1\Values106.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus110.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus111.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus112.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus113.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus200.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus201.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus203.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus210.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus211.dat; DestDir: {app}\MAGX1
Source: MAGX1\Modbus212.dat; DestDir: {app}\MAGX1
Source: MAGX1\DemoValues.dat; DestDir: {userappdata}\MAGX1

Source: MAGX2\MagX2.exe; DestDir: {app}\MAGX2
Source: MAGX2\MagX2.ico; DestDir: {app}\MAGX2
Source: MAGX2\MagX2_Error.dat; DestDir: {app}\MAGX2
Source: MAGX2\MagX2_Error_RU.dat; DestDir: {app}\MAGX2
Source: MAGX2\MagX2.dic; DestDir: {app}\MAGX2
Source: MAGX2\Menu2010.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2010.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2100.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2100.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2101.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2101.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2110.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2110.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2111.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2111.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2112.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2112.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2113.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2113.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2114.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2114.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2115.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2115.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2116.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2116.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2117.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2117.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2118.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2118.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2119.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2119.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2120.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2120.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2122.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2122.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2130.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2130.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2131.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2131.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2133.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2133.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2134.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2134.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2135.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2135.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2136.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2136.dat; DestDir: {app}\MAGX2
Source: MAGX2\Menu2137.dat; DestDir: {app}\MAGX2
Source: MAGX2\Modbus2137.dat; DestDir: {app}\MAGX2
Source: MAGX2\MAGX2_USB_VCOM.inf; DestDir: {app}\MAGX2
Source: MAGX2\DemoValues.dat; DestDir: {userappdata}\MAGX2
Source: MAGX2\Background.jpg; DestDir: {app}\MAGX2
Source: MAGX2\Logo.jpg; DestDir: {app}\MAGX2
Source: MAGX2\MagLogo.jpg; DestDir: {app}\MAGX2

Source: Flowmeters.exe; DestDir: {app}\

[Icons]
Name: {group}\Flowmeters; Filename: {app}\Flowmeters.exe
Name: {commondesktop}\Flowmeters; Filename: {app}\Flowmeters.exe

[Run]
Filename: {sys}\rundll32.exe; Parameters: setupapi,InstallHinfSection Uninstall 132 {app}\MAGX2\MAGX2_USB_VCOM.inf; Description: Install MAGX2 USB driver; Flags: postinstall nowait skipifsilent unchecked
Filename: {app}\Flowmeters.exe; Description: Launch application; Flags: postinstall nowait skipifsilent unchecked
