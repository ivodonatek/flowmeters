unit XModem;

interface

uses
  Windows, Messages, SysUtils, Classes, ExtCtrls, Forms, comctrls;

type
  // arreglo de bytes que conforman el mensaje modbus y un puntero al mismo
  TDataByte = array of byte;
  PByte     = ^byte;
  TXModemData = array[0..127] of byte;

  // COM Port Baud Rates
  TComPortBaudRate = ( br110, br300, br600, br1200, br2400, br4800,
                       br9600, br14400, br19200, br38400, br56000,
                       br57600, br115200, br128000, br256000 );
  // COM Port Numbers
  TComPortNumber = byte;
  // COM Port Data bits
  TComPortDataBits = ( db5BITS, db6BITS, db7BITS, db8BITS );
  // COM Port Stop bits
  TComPortStopBits = ( sb1BITS, sb1HALFBITS, sb2BITS );
  // COM Port Parity
  TComPortParity = ( ptNONE, ptODD, ptEVEN, ptMARK, ptSPACE );
  // COM Port Hardware Handshaking
  TComPortHwHandshaking = ( hhNONE, hhRTSCTS );
  // COM Port Software Handshaking
  TComPortSwHandshaking = ( shNONE, shXONXOFF );
  TComPortRtsControl = ( rcRTSEnable, rcRTSDISABLE, rcRTSToggle );

  PXModemPacket = ^TXModemPacket;
  TXModemPacket = record
        Header : byte;
        PacketNumber : byte;
        CPLPacketNumber : byte;
        Data : TXModemData;
        CRChi, CRClo : byte;
  end;

  TOnErrorEvent = procedure(Sender : TObject; Error : Byte) of Object;

  TXModem = class(TComponent)
  private
    FTimer                     : TTimer; // para contar el QueryTimeOut
    FResponseReady             : TNotifyEvent;// Evento que informa que se recibi� la respuesta de la consulta
    FOnError                   : TOnErrorEvent;// Evento que informa que se produjo un error
    FError                     : Byte;
    FBusy                      : Boolean;
    FComPortHandle             : THANDLE; // COM Port Device Handle
    FReadValues                : TDataByte;
    FWriteValues               : TDataByte;
    FComPort                   : TComPortNumber; // COM Port to use (1..4)
    FComPortBaudRate           : TComPortBaudRate; // COM Port speed (brXXXX)
    FComPortDataBits           : TComPortDataBits; // Data bits size (5..8)
    FComPortStopBits           : TComPortStopBits; // How many stop bits to use (1,1.5,2)
    FComPortParity             : TComPortParity; // Type of parity to use (none,odd,even,mark,space)
    FComPortHwHandshaking      : TComPortHwHandshaking; // Type of hw handshaking to use
    FComPortSwHandshaking      : TComPortSwHandshaking; // Type of sw handshaking to use
    fComPortRtsControl         : TComPortRtsControl;
    FComPortInBufSize          : word; // Size of the input buffer
    FComPortOutBufSize         : word; // Size of the output buffer
    FComPortPollingDelay       : word; // ms of delay between COM port pollings
    FEnableDTROnOpen           : boolean; { enable/disable DTR line on connect }
    FOutputTimeout             : word; { output timeout - milliseconds }
    FTempInBuffer              : pointer;
    FTimeout                   : word;      // Timeout del modbus
    FAbort                     : boolean;

    procedure SetComHandle( Value: THANDLE );
    procedure SetComPort( Value: TComPortNumber );
    procedure SetComPortBaudRate( Value: TComPortBaudRate );
    procedure SetComPortDataBits( Value: TComPortDataBits );
    procedure SetComPortStopBits( Value: TComPortStopBits );
    procedure SetComPortParity( Value: TComPortParity );
    procedure SetComPortHwHandshaking( Value: TComPortHwHandshaking );
    procedure SetComPortRtsControl( Value: TComPortRtsControl );
    procedure ApplyCOMSettings;
    //function Read( Sender: TObject; var Buff: TDataByte; DataSize: integer ):boolean;
    function Read(Sender: TObject; var Buff: TDataByte):integer;
    procedure OnTimeout(Sender: TObject); // para cuando ocurra el QueryTimeout
    { v1.02: returns the output buffer free space or 65535 if
             not connected }
    function OutFreeSpace: word;
    function InFreeSpace: word;
   { v1.02: flushes the rx/tx buffers }
    procedure FlushBuffers( inBuf, outBuf: boolean );
    { Send data }
    function SendPacket( XModemPacket:TXModemPacket ): boolean;
    procedure SetFTimeout(const Value: word);
    procedure SetComPortEnableDTROnOpen( Value: Boolean );
    function SendByte( Data:Byte ): boolean;
  public
    property WriteValues: TDataByte read FWriteValues write FWriteValues; // Arreglo de valores a escribir
    property ReadValues: TDataByte read FReadValues; // Arreglo de valores leidos
    property Error: Byte read FError;   // Byte de error del amo
    property Busy: Boolean read FBusy;   // Informa si el amo est� Busy
    function SendFromStream(Stream : TStream; ProgBar : TProgressBar):boolean;
    function SendFromFile(fileName: String; ProgBar : TProgressBar):boolean;

    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    // v1.02: make the Handle to the com port public (for TAPI...)
    property ComHandle: THANDLE read FComPortHandle write SetComHandle;
    function Connect: boolean;
    procedure Disconnect;
    function Connected: boolean;
    procedure Abort();

  published
    // Which COM Port to use
    property ComPort: TComPortNumber read FComPort write SetComPort default 2;
    // COM Port speed (bauds)
    property ComPortSpeed: TComPortBaudRate read FComPortBaudRate write SetComPortBaudRate default br9600;
    // Data bits to used (5..8, for the 8250 the use of 5 data bits with 2 stop bits is an invalid combination,
    // as is 6, 7, or 8 data bits with 1.5 stop bits)
    property ComPortDataBits: TComPortDataBits read FComPortDataBits write SetComPortDataBits default db8BITS;
    // Stop bits to use (1, 1.5, 2)
    property ComPortStopBits: TComPortStopBits read FComPortStopBits write SetComPortStopBits default sb1BITS;
    // Parity Type to use (none,odd,even,mark,space)
    property ComPortParity: TComPortParity read FComPortParity write SetComPortParity default ptNONE;
    // Hardware Handshaking Type to use:
    //  cdNONE          no handshaking
    //  cdCTSRTS        both cdCTS and cdRTS apply (** this is the more common method**)
    property ComPortHwHandshaking: TComPortHwHandshaking
             read FComPortHwHandshaking write SetComPortHwHandshaking default hhNONE;
    // Event to raise when there is a Response available
    property ComPortRtsControl: TComPortRtsControl
             read FComPortRtsControl write SetComPortRtsControl default rcRTSDISABLE;

    property OnResponseReady: TNotifyEvent read FResponseReady write FResponseReady;
    property OnError: TOnErrorEvent read FOnError write FOnError;
    property Timeout: Word read FTimeout write SetFTimeout;
    property EnableDTROnOpen: Boolean read FEnableDTROnOpen write SetComPortEnableDTROnOpen;
  end;

procedure Register;

implementation

const   SOH = $01;
        STX = $02;
        EOT = $04;
        ACK = $06;
        NAK = 21;
        CAN = 24;
        NGC = 67;

function CRC16(Bufferdata:TXModemData; Size:Integer):word;
var
    i,j:integer;
    u8Data:word;
    u16CRC:word;
begin
//    u16CRC:=$ffff;
    u16CRC:=0;
    // Repeat until all the data has been processed...
    for j:=0 to Size-1 do
    begin
        u8Data := Bufferdata[j];
        // XOR high byte of CRC with 8-bit data
        u16CRC := u16CRC xor ( u8Data shl 8);
        // Repeat 8 times
        for i:=8 downto 1 do
        begin
            // If highest bit is set, then shift left and XOR with 0x1021
            if (u16CRC and $8000) <> 0 then
                u16CRC := (u16CRC shl 1) xor $1021
            // else, just shift left
            else
                u16CRC := (u16CRC shl 1);
        end;
    end;
    result:= u16CRC;
end;

function TXModem.SendFromFile(fileName: String; ProgBar : TProgressBar):boolean;
var FS : TFileStream;
begin
  FS := TFileStream.Create(fileName,fmOpenRead);
  try
    Result:= SendFromStream(FS, ProgBar);
  finally
    FS.Free;
  end;
end;

function TXModem.SendFromStream(Stream : TStream; ProgBar : TProgressBar):boolean;
var
  XModemPacket: TXModemPacket;
  i, DataPageIndex : Integer;
  PacketNumber: Byte;
  Buff: TDataByte;
  Readed: Cardinal;
  ReadOK: Boolean;
  CRC : word;
  Error : byte;
begin
  Result:=false;
  FAbort:= false;
  if not connected then
  begin
    FError:=11;
    if Assigned(FOnError) then FOnError(Self, 11 );
    Exit;
  end;
  if FBusy then begin
    FError:=10;
    if Assigned(FOnError) then FOnError(self, 10);
    exit;
  end;

// inicializace
  FBusy:=True;
  FlushBuffers( true,true);
  FError:=0;

  PacketNumber :=1;
  DatapageIndex := 0;
  SetLength(Buff, 1);
  ReadOK := false;
  Stream.Position := 0;

//cekej na
  for i:=1 to 10 do
  begin
      Application.ProcessMessages();
      Readed:= Read(Self, Buff);
      if (Readed>0)and(Buff[readed-1] = NGC) then    //pokud dosel znak NGC
      begin
        ReadOK := true;
        Break;
      end;

      if FAbort then
      begin
        FBusy:=False;
        FAbort:=false;
        Result := false;
        exit;
      end;
  end;

  if not ReadOk then
  begin
    FError:=1;
    FBusy:=False;
    exit;
  end;

  while (DataPageIndex*128 < Stream.Size) do
  begin
    Error := 0;
    ReadOK:=false;
    Application.ProcessMessages();
    XModemPacket.Header := SOH;
    XModemPacket.PacketNumber := PacketNumber;
    XModemPacket.CPLPacketNumber := 255 - XModemPacket.PacketNumber;
    Stream.Seek(DataPageIndex*sizeof(XModemPacket.Data), soFromBeginning);
    Stream.Read(XModemPacket.Data, sizeof(XModemPacket.Data));
    CRC := CRC16(XModemPacket.Data, sizeof(XModemPacket.Data));
    XModemPacket.CRChi := CRC shr 8;
    XModemPacket.CRClo := CRC;
    while(Error<10)and(ReadOK=false) do
    begin
      if(FAbort) then
      begin
        for i:=0 to 7 do
          SendByte(CAN);
        FAbort := false;
        FBusy:=False;
        Result := false;
        exit;
      end;

      if SendPacket( XModemPacket ) then
      begin
        Readed:= Read(Self, Buff);
        if (readed > 0) then
        begin
          case Buff[0] of
            ACK:
            begin
              Inc(DataPageIndex);
              Inc(PacketNumber);
//              Stream.Seek(DataPageIndex*sizeof(XModemPacket.Data), soFromBeginning);
              ReadOK := true;
            end;
            CAN, EOT:
            begin
            //chyba cislovani paketu, konec prenosu
              FError:=3;
              FBusy:=False;
              exit;
            end;
          else
                Inc(Error);           //chyba, odesli paket znova
//                Stream.Seek(DataPageIndex*sizeof(XModemPacket.Data), soFromBeginning);
          end;
        end
        else
        begin
        //timeout
          FError:=2;
          FBusy:=False;
          exit;
        end;
      end
      else
      begin
        //timeout
          FError:=4;
          FBusy:=False;
          exit;
      end;

      if Assigned(ProgBar) then
      begin
        ProgBar.StepIt;
        Application.ProcessMessages();
      end;
    end;
    if Error >= 10 then
    begin
    //prekrocen limit chyb
      FError:=5;
      FBusy:=False;
      exit;
    end;
  end;

  SendByte(EOT);
  Readed:= Read(Self, Buff);
  if (readed > 0)and(Buff[0]=ACK) then
        result:= true;

  FBusy:=False;
  FAbort:=false;
end;

procedure TXModem.OnTimeout(Sender : TObject);
begin
  FTimer.Enabled:=false;
  FError:=7;
  FlushBuffers(true,true);
  if Assigned(FOnError) then FOnError(Self, FError);
  FBusy:=False;
end;

function TXModem.Read(Sender: TObject; var Buff: TDataByte):integer;
var t1:DWORD;
    ReadOK:Boolean;
    readed:cardinal;
    PBuff:PByte;
    i:integer;
begin
  Result := 0;
  t1 := GetTickCount;
  ReadOk:=false;
  while (DWORD(GetTickCount-t1) < FTimeout)and not ReadOK do //cekej na prijem data
  begin
//    if FComPortInBufSize - InFreeSpace >= DataSize then
    if FComPortInBufSize - InFreeSpace > 0 then
      ReadOK:=true;
    if(FAbort) then
    begin
      Result:= -1;
      exit;
    end;
  end;

  if not ReadOK then  //pokud neprisel predepsany pocet tak exit
    exit;
  ReadFile( FComPortHandle, FTempInBuffer^, FComPortInBufSize, readed, nil);       //cti data
  SetLength(Buff, readed );
  PBuff:=FTempInBuffer;
  for i:=0 to readed-1 do
  begin
    Buff[i]:=PBuff^;
    inc(PBuff);
  end;
//  if readed = DataSize then
    Result := readed;
end;

constructor TXModem.Create( AOwner: TComponent );
begin
  inherited Create( AOwner );
  // Initialize to default values
  FComPortHandle             := INVALID_HANDLE_VALUE;       // Not connected
  FComPort                   := 0;  // COM 0
  FComPortBaudRate           := br9600;  // 9600 bauds
  FComPortDataBits           := db8BITS; // 8 data bits
  FComPortStopBits           := sb1BITS; // 1 stop bit
  FComPortParity             := ptNONE;  // no parity
  FComPortHwHandshaking      := hhNONE;  // no hardware handshaking
  FComPortSwHandshaking      := shNONE;  // no software handshaking
  FComPortInBufSize          := 1028;    // input buffer of 1024 bytes
  FComPortOutBufSize         := 1028;    // output buffer of 1024 bytes
  FComPortPollingDelay       := 300;    // poll COM port every 1000ms
  FOutputTimeout             := 5000;    // output timeout - 5000ms
  FEnableDTROnOpen           := false;   // DTR high on connect
  // Temporary buffer for received data
  GetMem( FTempInBuffer, FComPortInBufSize ); //reserva memoria para el buffer de entrada
  // Allocate a window handle to catch timer's notification messages

  FTimeout                   := 1000;      // query timeout - 10000ms
  FResponseReady             := nil;       // evento sin implementar
  FTimer:=TTimer.Create(Self);             // Creamos el timer que contar� el QueryTimeout
  FTimer.Interval:=FTimeout;               // Asignamos el intervalo del timer = FQueryTimeout
  FTimer.OnTimer:=OnTimeout;                 // Asiganamos el m�todo Timeout
  FTimer.Enabled:=false;
  FError                     :=0;
  FOnError                   := nil;       // evento sin implementar
  FBusy                      :=False;
  FAbort  :=false;
end;

destructor TXModem.Destroy;
begin
  FTimer.Free;          //Liberamos el timer
  // Be sure to release the COM device
  Disconnect;
  // Free the temporary buffer
  FreeMem( FTempInBuffer, FComPortInBufSize );
  // Destroy the timer's window
  inherited Destroy;
end;

// v1.02: The COM port handle made public and writeable.
// This lets you connect to external opened com port.
// Setting ComPortHandle to 0 acts as Disconnect.
procedure TXModem.SetComHandle( Value: THANDLE );
begin
  // If same COM port then do nothing
  if FComPortHandle = Value then
    exit;
  { If value is $FFFFFFFF then stop controlling the COM port
    without closing in }
  if Value = $FFFFFFFF then
  begin
    { No more connected }
    FComPortHandle :=  INVALID_HANDLE_VALUE;
  end
  else
  begin
    { Disconnect }
    Disconnect;
    { If Value is = 0 then exit now }
    { (ComPortHandle := 0 acts as Disconnect) }
    if Value = 0  then
      exit;

    { Set COM port handle }
    FComPortHandle := Value;
  end;
end;

procedure TXModem.SetComPort( Value: TComPortNumber );
begin
  // Be sure we are not using any COM port
  if Connected then
    exit;
  // Change COM port
  FComPort := Value;
end;

procedure TXModem.SetComPortBaudRate( Value: TComPortBaudRate );
begin
  // Set new COM speed
  FComPortBaudRate := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TXModem.SetComPortDataBits( Value: TComPortDataBits );
begin
  // Set new data bits
  FComPortDataBits := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TXModem.SetComPortStopBits( Value: TComPortStopBits );
begin
  // Set new stop bits
  FComPortStopBits := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TXModem.SetComPortParity( Value: TComPortParity );
begin
  // Set new parity
  FComPortParity := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TXModem.SetComPortHwHandshaking( Value: TComPortHwHandshaking );
begin
  // Set new hardware handshaking
  FComPortHwHandshaking := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TXModem.SetComPortEnableDTROnOpen( Value: Boolean );
begin
  // Set new hardware handshaking
  FEnableDTROnOpen := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

procedure TXModem.SetComPortRtsControl( Value: TComPortRtsControl );
begin
  // Set new hardware handshaking
  fComPortRtsControl := Value;
  // Apply changes
  if Connected then
    ApplyCOMSettings;
end;

const
  Win32BaudRates: array[br110..br115200] of DWORD =
    ( CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400, CBR_4800, CBR_9600,
      CBR_14400, CBR_19200, CBR_38400, CBR_56000, CBR_57600, CBR_115200{v1.02 removed: CRB_128000, CBR_256000} );

const
  dcb_Binary              = $00000001;
  dcb_ParityCheck         = $00000002;
  dcb_OutxCtsFlow         = $00000004;
  dcb_OutxDsrFlow         = $00000008;
  dcb_DtrControlMask      = $00000030;
  dcb_DtrControlDisable   = $00000000;
  dcb_DtrControlEnable    = $00000010;
  dcb_DtrControlHandshake = $00000020;
  dcb_DsrSensivity        = $00000040;
  dcb_TXContinueOnXoff    = $00000080;
  dcb_OutX                = $00000100;
  dcb_InX                 = $00000200;
  dcb_ErrorChar           = $00000400;
  dcb_NullStrip           = $00000800;
  dcb_RtsControlMask      = $00003000;
  dcb_RtsControlDisable   = $00000000;
  dcb_RtsControlEnable    = $00001000;
  dcb_RtsControlHandshake = $00002000;
  dcb_RtsControlToggle    = $00003000;
  dcb_AbortOnError        = $00004000;
  dcb_Reserveds           = $FFFF8000;

// Apply COM settings.
procedure TXModem.ApplyCOMSettings;
var dcb: TDCB;
begin
  // Do nothing if not connected
  if not Connected then
    exit;

  // Clear all
  fillchar( dcb, sizeof(dcb), 0 );
  // Setup dcb (Device Control Block) fields
  dcb.DCBLength := sizeof(dcb); // dcb structure size
  dcb.BaudRate := Win32BaudRates[ FComPortBaudRate ]; // baud rate to use
  // Set fBinary: Win32 does not support non binary mode transfers
  // (also disable EOF check)
  dcb.Flags := dcb_Binary;
  if FEnableDTROnOpen then
    { Enabled the DTR line when the device is opened and leaves it on }
    dcb.Flags := dcb.Flags or dcb_DtrControlEnable;

  case FComPortHwHandshaking of // Type of hw handshaking to use
    hhNONE:; // No hardware handshaking
    hhRTSCTS: // RTS/CTS (request-to-send/clear-to-send) hardware handshaking
      dcb.Flags := dcb.Flags or dcb_OutxCtsFlow or dcb_RtsControlHandshake;
  end;
  case FComPortSwHandshaking of // Type of sw handshaking to use
    shNONE:; // No software handshaking
    shXONXOFF: // XON/XOFF handshaking
      dcb.Flags := dcb.Flags or dcb_OutX or dcb_InX;
  end;
  case fComPortRtsControl of // Type of hw handshaking to use
    rcRTSDISABLE:;
    rcRTSEnable:
      dcb.Flags := dcb.Flags or dcb_RtsControlEnable;
    rcRTSToggle:
      dcb.Flags := dcb.Flags or dcb_RtsControlToggle;
  end;

  dcb.XONLim := FComPortInBufSize div 4; // Specifies the minimum number of bytes allowed
                                         // in the input buffer before the XON character is sent
                                         // (or CTS is set)
  dcb.XOFFLim := 1; // Specifies the maximum number of bytes allowed in the input buffer
                    // before the XOFF character is sent. The maximum number of bytes
                    // allowed is calculated by subtracting this value from the size,
                    // in bytes, of the input buffer
  dcb.ByteSize := 5 + ord(FComPortDataBits); // how many data bits to use
  dcb.Parity := ord(FComPortParity); // type of parity to use
  dcb.StopBits := ord(FComPortStopbits); // how many stop bits to use
  dcb.XONChar := #17; // XON ASCII char
  dcb.XOFFChar := #19; // XOFF ASCII char
  SetCommState( FComPortHandle, dcb );
  { Flush buffers }
  FlushBuffers( true, true );
  // Setup buffers size
  SetupComm( FComPortHandle, FComPortInBufSize, FComPortOutBufSize );
end;

function TXModem.Connect: boolean;
var comName :string;
    tms: TCOMMTIMEOUTS;
begin
  // Do nothing if already connected
  Result := Connected;
  if Result then
    exit;
  // Open the COM port
  comName:= Format( '\\.\COM%d', [FComPort]);
  FComPortHandle := CreateFile( pchar(comName),
                                GENERIC_READ or GENERIC_WRITE,
                                0, // Not shared
                                nil, // No security attributes
                                OPEN_EXISTING,
                                FILE_ATTRIBUTE_NORMAL,
                                0 // No template
                              ) ;
  Result := Connected;
  if not Result then
    exit;
  // Apply settings
  ApplyCOMSettings;
  // Setup timeouts: we disable timeouts because we are polling the com port!
  tms.ReadIntervalTimeout := 1; // Specifies the maximum time, in milliseconds,
                                // allowed to elapse between the arrival of two
                                // characters on the communications line
  tms.ReadTotalTimeoutMultiplier := 0; // Specifies the multiplier, in milliseconds,
                                       // used to calculate the total time-out period
                                       // for read operations.
  tms.ReadTotalTimeoutConstant := 1; // Specifies the constant, in milliseconds,
                                     // used to calculate the total time-out period
                                     // for read operations.
  tms.WriteTotalTimeoutMultiplier := 0; // Spec<ifies the multiplier, in milliseconds,
                                        // used to calculate the total time-out period
                                        // for write operations.
  tms.WriteTotalTimeoutConstant := 0; // Specifies the constant, in milliseconds,
                                      // used to calculate the total time-out period
                                      // for write operations.
  SetCommTimeOuts( FComPortHandle, tms );
  // Start the timer (used for polling)
end;

procedure TXModem.Disconnect;
begin
  if Connected then
  begin
    // Release the COM port
    CloseHandle( FComPortHandle );
    // No more connected
    FComPortHandle :=  INVALID_HANDLE_VALUE;
  end;
end;

function TXModem.Connected: boolean;
begin
  Result := (FComPortHandle <> INVALID_HANDLE_VALUE);
end;

// v1.02: flish rx/rx buffers
procedure TXModem.FlushBuffers( inBuf, outBuf: boolean );
var dwAction: DWORD;
begin
  if not Connected then
    exit;
  // Flush the incoming data buffer
  dwAction := 0;
  if outBuf then
    dwAction := dwAction or PURGE_TXABORT or PURGE_TXCLEAR;
  if inBuf then
    dwAction := dwAction or PURGE_RXABORT or PURGE_RXCLEAR;
  PurgeComm( FComPortHandle, dwAction );
end;

function TXModem.InFreeSpace: word;
var stat: TCOMSTAT;
    errs: DWORD;
begin
  if not Connected then
    Result := 65535
  else
  begin
    ClearCommError( FComPortHandle, errs, @stat );
    Result := FComPortInBufSize - stat.cbInQue;
  end;
end;

// v1.02: returns the output buffer free space or 65535 if
//        not connected }
function TXModem.OutFreeSpace: word;
var stat: TCOMSTAT;
    errs: DWORD;
begin
  if not Connected then
    Result := 65535
  else
  begin
    ClearCommError( FComPortHandle, errs, @stat );
    Result := FComPortOutBufSize - stat.cbOutQue;
  end;
end;

// Send data
{ Send data (breaks the data in small packets if it doesn't fit in the output
  buffer) }
function TXModem.SendPacket( XModemPacket:TXModemPacket ): boolean;
var nToSend, DataSize: integer;
    nsent :dword;
    t1: longint;
begin
  { 0 bytes sent }
  Result := false;
  DataSize :=128+5;
//  DataSize :=sizeof(XModemPacket);    //nefunguje, kompilator doplni na sudy pocet
  { Do nothing if not connected }
  if not Connected then
    exit;
  { Current time }
  t1 := GetTickCount;
  { Loop until all data sent or timeout occurred }
  while (DataSize > 0) do
  begin
    if(FAbort) then
    begin
      Result := false;
      exit;
    end;
    { Get output buffer free space }
    nToSend := OutFreeSpace;
    { If output buffer has some free space... }
    if nToSend > 0 then
    begin
      { Don't send more bytes than we actually have to send }
      if nToSend > DataSize then
        nToSend := DataSize;
      { Send }
      WriteFile( FComPortHandle, XModemPacket, DataSize, nsent, nil );

      if nsent=0 then
        raise Exception.Create('Send error');

      { Decrease the count of bytes to send }
      DataSize := DataSize - abs(nsent);
      { Get current time }
      t1 := GetTickCount;
      { Continue. This skips the time check below (don't stop
        trasmitting if the FOutputTimeout is set too low) }
      continue;
    end;
    { Buffer is full. If we are waiting too long then
      invert the number of bytes sent and exit }
    if (GetTickCount-t1) > FOutputTimeout then
    begin
      Result := false;
      exit;
    end;
  end;
  Result:= DataSize = 0;
end;

function TXModem.SendByte( Data:Byte ): boolean;
var nsent:cardinal;
begin
//  Result := false;
  { Send }
  WriteFile( FComPortHandle, Data, 1, nsent, nil );
  if nsent=0 then
    raise Exception.Create('Send error');
  result:= nsent = 1;
end;

procedure Register;
begin
  { Register this component and show it in the 'System' tab
    of the component palette }
  RegisterComponents('Arkon', [TXModem]);
end;

procedure TXModem.SetFTimeout(const Value: word);
begin
  if ( (Value<>FTimeout ) and (Value>10)) then
  begin
    FTimeOut:=Value;
    FTimer.Interval:=FTimeOut;
  end;
end;

procedure TXModem.Abort();
begin
  FAbort  :=true;
end;

end.

