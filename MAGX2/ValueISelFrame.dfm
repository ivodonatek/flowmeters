inherited frameValueISel: TframeValueISel
  object Panel1: TPanel
    Left = 16
    Top = 48
    Width = 257
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 14670037
    TabOrder = 0
    object cbValues: TComboBox
      Left = 16
      Top = 12
      Width = 97
      Height = 20
      Style = csOwnerDrawFixed
      ItemHeight = 14
      TabOrder = 0
    end
    object btnWrite: TButton
      Left = 152
      Top = 8
      Width = 73
      Height = 25
      Caption = 'Write'
      TabOrder = 1
      OnClick = btnWriteClick
    end
  end
end
