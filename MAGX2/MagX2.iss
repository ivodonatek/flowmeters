; -- MagX2.iss --

[Setup]
AppName=MagX2
AppVerName=MagX2 2.0.0.15
AppPublisher=Arkon Flow Systems, s.r.o.
AppVersion=2.0.0.15
VersionInfoVersion=2.0.0.15
DefaultDirName={pf}\Arkon Flow Systems\MagX2
DefaultGroupName=Arkon Flow Systems\MagX2
UninstallDisplayIcon={app}\MagX2.exe
Compression=lzma
SolidCompression=yes

[Files]
Source: "MagX2.exe"; DestDir: "{app}"
Source: "MagX2.ico"; DestDir: "{app}"
Source: "MagX2_Error.dat"; DestDir: "{app}"
Source: "Menu2010.dat"; DestDir: "{app}"
Source: "Menu2100.dat"; DestDir: "{app}"
Source: "Modbus2010.dat"; DestDir: "{app}"
Source: "Modbus2100.dat"; DestDir: "{app}"
Source: "MAGX2_USB_VCOM.inf"; DestDir: "{app}"
Source: "DemoValues.dat"; DestDir: "{userappdata}\MagX2"

[Icons]
Name: "{group}\MagX2 SW"; Filename: "{app}\MagX2.exe"

[Run]
Filename: "{sys}\rundll32.exe"; Parameters: "setupapi,InstallHinfSection Uninstall 132 {app}\MAGX2_USB_VCOM.inf"; Description: "Install MagX2 driver"; Flags: postinstall nowait skipifsilent
Filename: "{app}\MagX2.exe"; Description: "Launch application"; Flags: postinstall nowait skipifsilent unchecked

