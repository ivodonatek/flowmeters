; -- MagX1.iss --

[Setup]
AppName=MagX1
AppVerName=MagX1 1.2.2.7
AppPublisher=Arkon Flow Systems, s.r.o.
AppVersion=1.2.2.7
VersionInfoVersion=1.2.2.7
DefaultDirName={pf}\Arkon Flow Systems\MagX1
DefaultGroupName=Arkon Flow Systems\MagX1
UninstallDisplayIcon={app}\MagX1.exe
Compression=lzma
SolidCompression=yes

[Files]
Source: MagX1.exe; DestDir: {app}
Source: MagX1.ico; DestDir: {app}
Source: Menu105.dat; DestDir: {app}
Source: Menu106.dat; DestDir: {app}
Source: Menu107.dat; DestDir: {app}
Source: Menu110.dat; DestDir: {app}
Source: Menu111.dat; DestDir: {app}
Source: Menu112.dat; DestDir: {app}
Source: Menu113.dat; DestDir: {app}
Source: Menu200.dat; DestDir: {app}
Source: Menu201.dat; DestDir: {app}
Source: Menu203.dat; DestDir: {app}
Source: Menu210.dat; DestDir: {app}
Source: Menu211.dat; DestDir: {app}
Source: Menu212.dat; DestDir: {app}
Source: Values105.dat; DestDir: {app}
Source: Values106.dat; DestDir: {app}
Source: Modbus110.dat; DestDir: {app}
Source: Modbus111.dat; DestDir: {app}
Source: Modbus112.dat; DestDir: {app}
Source: Modbus113.dat; DestDir: {app}
Source: Modbus200.dat; DestDir: {app}
Source: Modbus201.dat; DestDir: {app}
Source: Modbus203.dat; DestDir: {app}
Source: Modbus210.dat; DestDir: {app}
Source: Modbus211.dat; DestDir: {app}
Source: Modbus212.dat; DestDir: {app}
Source: DemoValues.dat; DestDir: {userappdata}\MagX1

[Icons]
Name: {group}\MagX1 SW; Filename: {app}\MagX1.exe
Name: {commondesktop}\MagX1 SW; Filename: {app}\MagX1.exe

[Run]
Filename: {app}\MagX1.exe; Description: Launch application; Flags: postinstall nowait skipifsilent unchecked
